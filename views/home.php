<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Workshop-02</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/materialize.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    
    <div class="container">
        <h2>Countries and their capitals</h2>
        <br>
        <?php foreach($countries as $y=>$y_value):?> 
        <h6>The capital of <?php echo $y;?> is: <?php echo $y_value;?></h6>
        <br>

        <?php endforeach;?>
        <h2>Temperature range</h2><br>
        <h6>The average temperature is: <?php echo $total;?></h6><br>
        <h6>The 5 lowest temperatures: <?php echo $lowest[0].", ",$lowest[2].", ",$lowest[3].", ",$lowest[4].", ",$lowest[5];?></h6><br>
        <h6>The 5 highest temperatures: <?php echo $hightest[0].", ",$hightest[2].", ",$hightest[3].", ",$hightest[4].", ",$hightest[5]?></h6><br>
        
    </div>


</body>
</html>

